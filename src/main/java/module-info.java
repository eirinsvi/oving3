module no.ntnu.eirinsvi {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.eirinsvi to javafx.fxml;
    exports no.ntnu.eirinsvi;
}