package no.ntnu.eirinsvi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.io.IOException;


/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        BorderPane root = FXMLLoader.load((getClass().getResource("primary.fxml")));
        stage.setScene( new Scene(root, 640, 470));
        stage.setTitle("card-game");
        stage.show();
    }

    public static void main(String[] args) { launch(); }

}