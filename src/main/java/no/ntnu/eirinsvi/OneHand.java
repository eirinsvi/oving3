package no.ntnu.eirinsvi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OneHand {
    private ArrayList<PlayingCard> oneHand = new ArrayList<>();

    public OneHand(){ }

    @Override
    public String toString() {
        String oneHandText = "";
        for(PlayingCard p : oneHand) {
            oneHandText += p.getAsString() + " ";
        }
        return oneHandText;
    }

    public void addPlayingCard(PlayingCard playingcard){
        if(!oneHand.contains(playingcard)) {
            oneHand.add(playingcard);
        }
    }

    public ArrayList<PlayingCard> getOneHand(){
        return oneHand;
    }

    public int sumOfHand(){
        return oneHand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    public String getHearts(){
        List<String> hearts = oneHand.stream().filter(p -> p.getSuit() == 'H')
                .map(PlayingCard::getAsString)
                .collect(Collectors.toList());
        if(hearts.isEmpty()){
            return "No Hearts";
        } else{
            return String.join(" ", hearts);
        }
    }


    public boolean searchForSpadeQueen(){
        return oneHand.stream().anyMatch(p -> p.getAsString().equals("S12"));
    }

    public boolean flush() throws IllegalArgumentException{
        if(oneHand.size()>4) {
            return oneHand.stream().map(PlayingCard::getSuit).distinct().count() <= 1;
        } else {
            throw new IllegalArgumentException("The hand must contain at least 5 cards.");
        }
    }
}
