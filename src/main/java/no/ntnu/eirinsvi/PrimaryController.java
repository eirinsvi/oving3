package no.ntnu.eirinsvi;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class PrimaryController{
    DeckOfCards deck = new DeckOfCards();
    OneHand hand = deck.dealHand(5);


    @FXML
    private Label oneHand;

    @FXML
    private Label sumOfFaces;

    @FXML
    private Label cardsOfHearts;

    @FXML
    private Label isFlush;

    @FXML
    private Label hasQueenOfSpades;


    @FXML
    private void clickedDealHandButton(){
        oneHand.setText(String.valueOf(hand));
    }

    @FXML
    private void clickedCheckHandButton(){
        getSumOfFaces();
        showCardsOfHearts();
        isFlush();
        hasQueenOfSpades();
    }

    @FXML
    private void getSumOfFaces(){
        sumOfFaces.setText(String.valueOf(hand.sumOfHand()));
    }

    @FXML
    private void showCardsOfHearts(){
        cardsOfHearts.setText(String.valueOf(hand.getHearts()));
    }

    @FXML
    private void isFlush(){
        isFlush.setText(String.valueOf(hand.flush()));
    }

    @FXML
    private void hasQueenOfSpades(){
        hasQueenOfSpades.setText(String.valueOf(hand.searchForSpadeQueen()));
    }


}
