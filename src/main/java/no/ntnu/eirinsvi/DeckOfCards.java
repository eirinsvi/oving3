package no.ntnu.eirinsvi;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    Random random= new Random();
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

    public DeckOfCards(){
        for(int i=1; i<14; i++){
            for(int j=0; j<4; j++)
                deckOfCards.add(new PlayingCard(suit[j], i));
        }
    }
    public OneHand dealHand(int n) throws IllegalArgumentException{
        if(n>0 && n<53){
            ArrayList<PlayingCard> clone = (ArrayList<PlayingCard>) deckOfCards.clone();
            OneHand hand = new OneHand();
            for(int i = 0; i<n; i++){
                PlayingCard randomPlayingCard = clone.get(random.nextInt(52));
                hand.addPlayingCard(randomPlayingCard);
                clone.remove(randomPlayingCard);
            }
            return hand;
        } else{
            throw new IllegalArgumentException("You must deal at least one card, and not more than 52.");
        }
    }
}
