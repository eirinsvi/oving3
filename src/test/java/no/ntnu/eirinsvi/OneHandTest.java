package no.ntnu.eirinsvi;

import no.ntnu.eirinsvi.OneHand;
import no.ntnu.eirinsvi.PlayingCard;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OneHandTest {
    OneHand oneHand = new OneHand();

    @Nested
    class OneHandFunctionalities {
        @Test
        public void sumOfFaces() {
            oneHand.getOneHand().add(new PlayingCard('S', 13));
            assertEquals(13, oneHand.sumOfHand());
        }

        @Test
        public void getHeartsPrintStringWithCorrectCards() {
            oneHand.getOneHand().add(new PlayingCard('S', 13));
            oneHand.getOneHand().add(new PlayingCard('H', 7));
            oneHand.getOneHand().add(new PlayingCard('H', 11));
            assertEquals("H7 H11", oneHand.getHearts());
        }
        @Test
        public void oneHandContainQueenOfSpade(){
            oneHand.getOneHand().add(new PlayingCard('S', 12));
            assertTrue(oneHand.searchForSpadeQueen());
        }
        @Test
        public void oneHandDontContainQueenOfSpade(){
            oneHand.getOneHand().add(new PlayingCard('S', 13));
            assertFalse(oneHand.searchForSpadeQueen());
        }
        @Test
        public void oneHandHasFlush(){
            oneHand.getOneHand().add(new PlayingCard('S', 1));
            oneHand.getOneHand().add(new PlayingCard('S', 2));
            oneHand.getOneHand().add(new PlayingCard('S', 3));
            oneHand.getOneHand().add(new PlayingCard('S', 4));
            oneHand.getOneHand().add(new PlayingCard('S', 5));
            assertTrue(oneHand.flush());
        }
        @Test
        public void oneHandDoesntHaveFlush(){
            oneHand.getOneHand().add(new PlayingCard('S', 1));
            oneHand.getOneHand().add(new PlayingCard('S', 2));
            oneHand.getOneHand().add(new PlayingCard('S', 3));
            oneHand.getOneHand().add(new PlayingCard('S', 4));
            oneHand.getOneHand().add(new PlayingCard('C', 5));
            assertFalse(oneHand.flush());
        }
        @Test
        public void oneHandHasTooFewCardsForFlush(){
            oneHand.getOneHand().add(new PlayingCard('S', 1));
            assertThrows(IllegalArgumentException.class, () -> oneHand.flush());
        }

    }
}
