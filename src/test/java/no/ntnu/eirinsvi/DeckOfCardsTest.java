package no.ntnu.eirinsvi;

import no.ntnu.eirinsvi.DeckOfCards;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    DeckOfCards deckOfCards = new DeckOfCards();

    @Nested
    class dealHandMethodWorksWithLegalAndIllegalValues {
        @Test
        public void dealHandBetweenOneAndFiftytwoCards() {
            assertEquals(3, deckOfCards.dealHand(3).getOneHand().size());
        }

        @Test
        public void dealHandWithNegativAmountOfCards() {
            assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(-1));
        }
    }
}
